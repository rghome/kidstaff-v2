<?php

require 'vendor/autoload.php';

use ElephantIO\Client;
use ElephantIO\Engine\SocketIO\Version1X;

$pass = '15a024315a0a06517ba794ade71c8fe8';

$client = new Client(new Version1X('http://localhost:3032?pass=' . $pass));

/**
 * @param string $channel
 * @param string $message
 * @param bool $storable
 *
 * @return mixed
 */
function sendMessage($channel, $message, $storable = true) {
    global  $client;

    $data = [
        'channel'       => $channel,
        'created_at'    => (new DateTime)->getTimestamp(),
        'message'       => $message,
        'storable'      => $storable
    ];

    $client->initialize();
    $client->emit('event:new', $data);
    $client->close();

    return $data;
}

print_r(sendMessage('comment', 'from php'));

