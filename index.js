/**
 * Created by rg on 21.11.16.
 */
var io = require('socket.io'),
    time = require('time'),
    socket = io.listen(3032),
    db = require('./db');

var phpClientCookieName = 'php_user';
var cookieName = 'user';


process.argv.forEach(function (val, index, array) {
    if (val === '--secure') {
        var pass = '15a024315a0a06517ba794ade71c8fe8';

        socket.use(function(client, next) {
            var handshake = client.handshake;

            if (handshake.query.hasOwnProperty('pass')) {
                if (handshake.query.pass === pass) {
                    next(null, true);
                }
            }

            next(new Error('Passcode error'));
        });
    }
});

/**
 * User Auth
 */
socket.use(function(client, next) {
    var handshake = client.handshake;

    if (handshake.query !== undefined && handshake.query[phpClientCookieName] !== undefined) {
        var date = new Date();
        date.setTime(date.getTime()+(1*24*60*60*1000));
        var expires = "; expires="+date.toGMTString();

        handshake.headers.cookie = cookieName + "="+handshake.query[phpClientCookieName]+expires+"; path=/";

        client.authUser = parseInt(handshake.query[phpClientCookieName]);
    } else {
        var list = {},
            rc = client.request.headers.cookie;

        rc && rc.split(';').forEach(function( cookie ) {
            var parts = cookie.split('=');
            list[parts.shift().trim()] = decodeURI(parts.join('='));
        });

        if (list[cookieName]) {
            client.authUser = parseInt(list[cookieName]);
        }
    }

    next(null, true);
});

/**
 * Подключение к БД
 *
 * @type {any}
 */
var connect = db.init('mongodb://localhost/kidstaff').connect();

/**
 * Загрузчик
 *
 * @param options
 * @returns {Run}
 * @constructor
 */
function Run(options) {
    this.models = [];
    this.client = options.client;
    this.socket = options.socket;
    this.db = connect;

    var normalizedPath = require('path').join(__dirname, './model'),
        modelsList = require("fs").readdirSync(normalizedPath);

    for (var modelIndex in modelsList) {
        var file = modelsList[modelIndex];
        var model = require('./model/' + file);

        if (model.hasOwnProperty('name')) {
            this.models[file.split('.')[0]] = new model.load(this);
        }
    }

    return this;
};


socket.on('connection', function(client) {
    /**
     * Загрузка прилодения
     *
     * @type {Run}
     */
    var application = new Run({
        socket: socket,
        client: client
    });

    /**
     * Список слушателей
     *
     * @type {{**}}
     */
    var controllers = {
        'event:new': function (data) {
            return application.models.event.newEvent(data);
        },
        'event:updates': function (data, cl) {
            return application.models.event.updates(data, cl);
        },
        'channel:join': function (channel, cl) {
            return application.models.channel.join(channel, cl);
        },
        'channel:leave': function (channel, cl) {
            return application.models.channel.leave(channel, cl);
        },
        'channel:join:private': function (channel, cl) {
            return application.models.channel.joinPrivate(channel, cl);
        },
        'channel:leave:private': function (channel, cl) {
            return application.models.channel.leavePrivate(channel, cl);
        }
    };

    /**
     * Загрузка слушателей
     */
    for (var controller in controllers) {
        client.on(controller, controllers[controller]);
    }
});