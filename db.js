/**
 * Created by rg on 04.12.16.
 */

var mongoose = require('mongoose'),
    time = require('time');

/**
 *
 * @type {{uri: undefined, mongoose: undefined, schemas: {}, models: {}, init: module.exports.init, connect: module.exports.connect, loadSchemas: module.exports.loadSchemas, loadModels: module.exports.loadModels, getSchemas: module.exports.getSchemas, cursorConverter: module.exports.cursorConverter}}
 */
module.exports = {
    /**
     * URI MongoDB
     */
    uri: undefined,
    /**
     * Mongoose курсор
     */
    mongoose: undefined,
    /**
     * Список схем
     */
    schemas: {},
    /**
     * Список моделей
     */
    models: {},
    /**
     * Предварительная инициализация
     *
     * @param uri
     * @returns {exports}
     */
    init: function (uri) {
        this.uri = uri;

        return this;
    },
    /**
     * Создание подключения
     *
     * @returns {exports}
     */
    connect: function () {
        this.mongoose = mongoose.connect(this.uri);
        this.loadSchemas();
        this.loadModels();

        return this;
    },
    /**
     * Загрузка схем
     *
     * @returns {exports}
     */
    loadSchemas: function () {
        this.schemas['event'] = new this.mongoose.Schema({
            channel: String,
            message: String,
            created_at: {
                type: Date,
                expires: 2592000, // expire after 30 days
                default: new time.Date().setTimezone('Europe/Kiev')
            }
        }, { strict: false });

        this.schemas['channel'] = new this.mongoose.Schema({
            name: String,
            users: Array,
            private: Boolean
        }, { strict: false });

        return this;
    },
    /**
     * Загрузка моделей
     *
     * @returns {exports}
     */
    loadModels: function () {
        this.models['event'] = this.mongoose.model('event', this.schemas['event']);
        this.models['channel'] = this.mongoose.model('channel', this.schemas['channel']);

        return this;
    },
    /**
     * Все схемы
     *
     * @returns {{}}
     */
    getSchemas: function () {
        return this.schemas;
    },
    /**
     * Конвертрер курсора
     *
     * @param cursor
     * @returns {*}
     */
    cursorConverter: function (cursor) {
        return new time.Date(cursor * 1000)
            .setTimezone('Europe/Kiev')
            // .getTime()
        ;
    }
};