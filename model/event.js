
var time = require('time');

/**
 * Export
 *
 * @type {{name: string, load: EventModel}}
 */
module.exports = {
    name: 'EventModel',
    load: EventModel
};

/**
 * @param application
 * @returns {EventModel}
 * @constructor
 */
function EventModel(application) {
    this.app = application;
    this.model = this.app.db.models['event'];

    return this;
};

/**
 * Получение нового события
 *
 * @param data
 */
EventModel.prototype.newEvent = function (data) {
    var $this = this;
    if (data.storable === true) {
        data['created_at'] = this.app.db.cursorConverter(data['created_at']);
        var rec = new this.model(data);

        rec.save(function (err, row) {
            $this.app.socket.sockets
                .in(row.channel)
                .emit('get new event', row)
            ;
        });
    } else {
        $this.app.socket.sockets
            .in(data.channel)
            .emit('get new event', data)
        ;
    }
};

/**
 * Получение обновлений
 *
 * @param data
 * @param cl
 */
EventModel.prototype.updates = function (data, cl) {
    var cursorTime = this.app.db.cursorConverter(data.cursor);

    this.model.find({
        $and: [
            { channel: { $in: Object.keys(this.app.client.rooms) } },
            { created_at: { $gt: cursorTime } }
        ]
    }, function (err, data) {
        cl(data);
    });
};