var SocketService = {
    /**
     * Флаг обеспечивающий повторное переподключение
     * при разрыве соединения с сервером
     */
    reconnect: false,
    /**
     * Текущий сокет
     */
    socket: null,
    /**
     * Подключение
     *
     * @param uri
     */
    connect: function (uri) {
        this.socket = io.connect(uri, {
            reconnection: false,
            reconnectionDelay: 1,
            reconnectionDelayMax: 10000,
            reconnectionAttempts: 1,
            timeout: 100
        });

        var onlineChecker = function () {
            if (navigator.onLine === false) {
                // SocketService.socket.io.reconnect();

                clearInterval(onlineCheckerInterval);
            }
        };

        var onlineCheckerInterval = setInterval(onlineChecker, 1000);
    },
    /**
     * Сервис для работы со слушателями
     */
    on: {
        /**
         * Список слушателей
         */
        handlers: { },
        /**
         * Вызов одного отдельного слушателя
         *
         * @param name
         * @returns {*|{handlers, get, generate}}
         */
        get: function (name) {
            return SocketService.socket.on(name, this.handlers[name]);
        },
        /**
         * Автоматический вызов всех доступных слушателей
         */
        generate: function () {
            var generateAction = function() {
                if (SocketService.socket.connected) {
                    Object.keys(SocketService.on.handlers).map(function(name) {
                        SocketService.socket.on(name, SocketService.on.handlers[name]);
                    });
                    clearInterval(generateInterval);
                }
            };
            var generateInterval = setInterval(generateAction, 100);
        }
    },
    /**
     * Сервис для работы с курсором
     */
    cursor: {
        /**
         * Текущее значение
         */
        point: null,
        /**
         * Получить курсор
         *
         * @returns {null}
         */
        get: function() {
            if (this.point === null) {
                this.set();
            }

            return this.point;
        },
        /**
         * Установить курсор. Если значение не указанно –
         * будет использовано значение с хранилища или будет
         * сгенерировано новое значение
         *
         * @param point
         * @returns {null}
         */
        set: function (point) {
            var $this = this;

            if (point === undefined) {
                var cursor = localStorage.getItem('cursor');
                if (cursor === null) {
                    var date = new Date();
                    date.setSeconds(date.getSeconds() + 5);
                    $this.point = Math.floor(date.getTime() / 1000);
                    localStorage.setItem('cursor', $this.point);
                } else {
                    this.point = parseInt(cursor);
                }
            } else {
                this.point = point;
            }

            return this.point;
        },
        /**
         * Обновление курсора
         *
         * @returns {*|null}
         */
        refresh: function () {
            localStorage.removeItem('cursor');
            return this.set();
        }
    },
    /**
     * Сервис для работы с каналами
     */
    channel: {
        /**
         * Подключение к каналу.
         * Если private установлен в true – сервер будет
         * подключаться к приватному каналу, соотвтеснно, если false –
         * к публичному.
         *
         * @param channel
         * @param private
         * @param cl
         * @returns {Emitter|Socket|*}
         */
        join: function (channel, private, cl) {
            return SocketService.socket.emit('channel:join' + (private ? ':private' : ''), channel, cl);
        },
        /**
         * Отключения от канала
         *
         * @param channel
         * @param private
         * @param cl
         * @returns {Emitter|Socket|*}
         */
        leave: function (channel, private, cl) {
            return SocketService.socket.emit('channel:leave' + (private ? ':private' : ''), channel, cl);
        }
    },
    timer: {
        timers: { },
        inc:0,
        step: 0,
        values: [1000, 5000, 10000],
        currentValue: 0,
        start: function(cb, gap) {
            var key = SocketService.timer.inc;

            SocketService.timer.inc++;
            SocketService.timer.timers[key] = [
                setInterval(cb, gap), cb
            ];

            return [key, cb];
        },
        stop: function(id) {
            if (!SocketService.timer.timers[id]) {
                return false;
            }

            SocketService.timer.inc = 0;
            SocketService.timer.step = 0;
            SocketService.timer.currentValue = 0;
            clearInterval(SocketService.timer.timers[id][0]);
            delete SocketService.timer.timers[id];
        },
        change: function(id, cb, gap) {
            SocketService.timer.stop(0);

            if(!SocketService.timer.timers[id]) {
                SocketService.timer.timers[id] = [
                    setInterval(cb, gap), cb
                ];
            }
        }
    }
};