/**
 * Export
 *
 * @type {{name: string, load: ChannelModel}}
 */
module.exports = {
    name: 'ChannelModel',
    load: ChannelModel
};

/**
 * @param application
 * @returns {ChannelModel}
 * @constructor
 */
function ChannelModel(application) {
    this.app = application;
    this.model = this.app.db.models['channel'];

    return this;
};

/**
 * Присоединится к каналу
 *
 * @param channel
 * @param cl
 */
ChannelModel.prototype.join = function (channel, cl) {
    var $this = this;
    this.app.client.join(channel, function () {
        cl({current: channel, channels: $this.app.client.rooms});
    });
};

/**
 * Присоединится к приватному каналу
 *
 * @param channel
 * @param cl
 */
ChannelModel.prototype.joinPrivate = function (channel, cl) {
    var $this = this;
    var user = this.app.client.authUser || null;

    if (user === null) return false;

    var handler = function (err, data) {
        if (data.length) {
            $this.app.client.join(channel, function () {
                cl({
                    current: channel,
                    channels: $this.app.client.rooms,
                    privateChannel: data
                });
            });
        }
    };

    this.model.find({
        $and: [
            { name: channel },
            { users: { $in: [user] } }
        ]
    }, handler);
};

/**
 * Покинуть приватный канал
 *
 * @param channel
 * @param cl
 */
ChannelModel.prototype.leavePrivate = function (channel, cl) {
    var $this = this;
    var user = this.app.client.authUser || null;

    if (user === null) return false;

    var handler = function (err, data) {
        if (data.toObject().hasOwnProperty('_id')) {
            $this.app.client.leave(channel, function () {
                cl({
                    current: channel,
                    channels: $this.app.client.rooms,
                    privateChannel: data
                });
            });
        }
    };

    this.model.findOneAndUpdate({ name: channel }, {
        $pull: { users: user }
    }, handler);
};

/**
 * Покинуть канал
 *
 * @param channel
 * @param cl
 */
ChannelModel.prototype.leave = function (channel, cl) {
    var $this = this;
    this.app.client.leave(channel, function () {
        cl({current: channel, channels: $this.app.client.rooms});
    });
};