<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \Interop\Container\ContainerInterface;
use Dflydev\FigCookies\FigResponseCookies;
use Dflydev\FigCookies\FigRequestCookies;
use Dflydev\FigCookies\Cookie;
use Dflydev\FigCookies\SetCookie;
use ElephantIO\Engine\SocketIO\Version1X;
use \RKA\Session;
use \MongoDB\Driver\Manager;

require 'vendor/autoload.php';

session_start();

$config = [];
require 'config.php';

$app = new \Slim\App(["settings" => $config]);

// Get container
$container = $app->getContainer();

/**
 * @param ContainerInterface $container
 *
 * @return \Slim\Views\Twig
 */
$container['view'] = function (ContainerInterface $container) use ($config) {
    $view = new \Slim\Views\Twig('views');

    // Instantiate and add Slim specific extension
    $basePath = rtrim(str_ireplace('index.php', '', $container['request']->getUri()->getBasePath()), '/');
    $view->addExtension(new Slim\Views\TwigExtension($container['router'], $basePath));
    $view->getEnvironment()->addGlobal('session', $_SESSION);
    $view->getEnvironment()->addGlobal('config', $config);

    $view->getEnvironment()->addGlobal('channels', $config['channels']);

    return $view;
};

/**
 * @param ContainerInterface $container
 *
 * @return \ElephantIO\Client
 */
$container['ws'] = function (ContainerInterface $container) {
    $settings = $container['settings']['ws'];

    $url = 'http://' . $settings['host'] . ':' . $settings['port'] . '/?php_user=' . $_SESSION['user']['id'];
    return new ElephantIO\Client(new Version1X($url));
};

$container['mongo'] = function (ContainerInterface $container) {
    return (new MongoDB\Client("mongodb://127.0.0.1:27017"))->kidstaff;
};

$app->add(new \RKA\SessionMiddleware(['name' => 'kidstaff']));

$app->add(function (Request $request, Response $response, callable $next) use ($config) {
    $cookies = $request->getCookieParams();

    if (isset($cookies['user'])) {
        foreach ($config['users'] as $user) {
            if ($user['id'] == $cookies['user']) {
                $_SESSION['user'] = $user;
            }
        }
    }

    return $next($request, $response);
});

$app->get('/', function (Request $request, Response $response) use ($container) {
    $privateChannels = function () use ($container) {
        if (isset($_SESSION['user'])) {
            /** @var \MongoDB\Collection $channels */
            $channelsCollection = $container['mongo']->channels;

            return iterator_to_array($channelsCollection->find(['users' => ['$in' => [$_SESSION['user']['id']]]]));
        }

        return null;
    };

    return $this->view->render($response, 'index.twig', [
        'privateChannels' => $privateChannels()
    ]);
})->setName('index');

$app->post('/login', function(Request $request, Response $response) use ($config) {
    $data = $request->getParsedBody();

    foreach ($config['users'] as $user) {
        if (
            ($user['name'] === $data['name']) &&
            ($user['password'] === $data['password'])
        ) {
            echo json_encode($user);
        }
    }
})->setName('login');

$app->post('/newPrivateChannel', function (Request $request, Response $response) use ($container) {
    $data = $request->getParsedBody();

    /** @var \MongoDB\Collection $channels */
    $channels = $container['mongo']->channels;


    foreach ($data['users'] as &$userId) {
        $userId = (int) $userId;
    }
    // Add creator
    $data['users'][] = $_SESSION['user']['id'];

    $data['private'] = true;

    $insertOneResult = $channels->insertOne($data);

    echo json_encode($insertOneResult->getInsertedId());
})->setName('newPrivateChannel');

$app->run();
