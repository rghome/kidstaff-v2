/**
 * Created by rg on 21.11.16.
 */
module.exports = function(grunt) {
    grunt.initConfig({
        pkg : grunt.file.readJSON('package.json'),
        // jshint : {
        //     myFiles : ['./Server/<strong>/*.js','./Routes/</strong>/*.js']
        // },
        jshint: {
            myFiles: ['./index.js'] //, './src/*.js', './src/<strong>/*.js'
        },
        nodemon : {
            script : './index.js'
        }
    });
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-nodemon');
    grunt.registerTask('default', ['jshint','nodemon']);
};