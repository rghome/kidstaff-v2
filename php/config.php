<?php

$config['displayErrorDetails'] = true;
$config['addContentLengthHeader'] = true;
$config['determineRouteBeforeAppMiddleware'] = true;

date_default_timezone_set('Europe/Kiev');

$config['ws']['host']   = "localhost";
$config['ws']['port']   = "3032";

$config['channels'] = [
    'comment', 'message', 'reply'
];

$config['users'] = [
    [
        'id' => 1,
        'name' => 'admin',
        'password' => 'admin'
    ],
    [
        'id' => 2,
        'name' => 'user1',
        'password' => 'user1'
    ],
    [
        'id' => 3,
        'name' => 'user2',
        'password' => 'user2'
    ],
    [
        'id' => 4,
        'name' => 'user3',
        'password' => 'user3'
    ],
    [
        'id' => 5,
        'name' => 'user4',
        'password' => 'user4'
    ],
];